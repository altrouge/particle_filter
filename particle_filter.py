import numpy as np

class SISParticleFilter:
    def __init__(self, number_of_particles, init_particle):
        self.m_particles = number_of_particles*[init_particle]
        pass

    def predict(self, control):
        next_particles = []
        for particle in self.m_particles:
            next_particles.append(control.compute(particle))

        self.m_particles = next_particles

    def update(self, measurement):

        # assign weight based on probability of measurement at particle
        weights = []
        for particle in self.m_particles:
            weights.append(measurement.unscaledProbabilityFrom(particle))

        SISParticleFilter._normalizeProbabilities(weights)

        # resample depending on weight
        new_particles = []
        for _ in range(self.particles):
            new_particles.append(np.random.choice(self.particles, 1, replace=False, p=weights))

        self.m_particles = new_particles

    def particles(self):
        return self.m_particles

    @staticmethod
    def _normalizeProbabilities(weights):
        norm = np.linalg.norm(weights, ord=1)
        weights = weights/norm
