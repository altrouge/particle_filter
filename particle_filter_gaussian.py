import numpy as np

class LinearMatrixWrapper:
    def __init__(self, A):
        self.A = A

    def __call__(self, x):
        return self.A.dot(x)

class GaussianMeasurement:
    def __init__(self, hx2z, z, covariance_inv):
        self.hx2z=hx2z
        self.z=z
        self.covariance_inv=covariance_inv

    def unscaledProbabilityFrom(self, x):
        Hx = self.hx2z(x)
        residual = (self.z-Hx).reshape(self.dim(), 1)
        return np.exp(-np.transpose(residual).dot(self.covariance_inv.dot(residual)))

    def dim(self):
        return len(self.z)

class GaussianPrediction:
    def __init__(self, f, covariance):
        # TODO checks on inputs
        self.f = f
        # cov = L L^T
        self.L = np.linalg.cholesky(covariance)

    def compute(self, x):
        # TODO checks on inputs
        fx = self.f(x)
        random_vec = np.random.normal(size=self.dim())
        random_vec = self.L.T.dot(random_vec)
        return fx+random_vec


    def dim(self):
        return self.L.shape[0]
