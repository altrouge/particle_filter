import particle_filter_gaussian as pfg
import particle_filter as pf

from scipy.stats import ortho_group

import numpy as np

import unittest

class TestHelpers():
    @staticmethod
    def generateRandomCovarianceMatrix(n):
        rot = TestHelpers.generateRandomRotationMatrix(n)
        diagonal = np.array([abs(v) for v in np.random.normal(size=n)])
        rd = rot.dot(np.diag(diagonal))

        return rd.dot(rd.T)

    @staticmethod
    def generateRandomRotationMatrix(n):
        return ortho_group.rvs(n)

class TestGaussian(unittest.TestCase):

    def test_matrix_wrapper(self):
        A = np.array([[1,2,3],[4,5,6]])
        Awrap = pfg.LinearMatrixWrapper(A)
        x = np.array([1,2,3])

        np.testing.assert_array_equal(Awrap(x), A.dot(x))

    def test_dim_gaussian_measurement(self):
        A = np.array([[1,2,3],[4,5,6]])
        hx2z = pfg.LinearMatrixWrapper(A)
        z = np.array([1,2])
        covariance_inv= np.array([
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1]
            ])

        gm = pfg.GaussianMeasurement(hx2z=hx2z,z=z,covariance_inv=covariance_inv)

        self.assertEqual(gm.dim(), 2)

    def test_dim_gaussian_prediction(self):
        A = np.array([[1.0,2.0,3.0],[4.0,5.0,6.0],[7.0,8.0,9.0]])
        f = pfg.LinearMatrixWrapper(A)
        covariance= np.array([
            [1, 0, 0],
            [0, 1, 0],
            [0, 0, 1]
            ])

        gp = pfg.GaussianPrediction(f=f,covariance=covariance)

        self.assertEqual(gp.dim(), 3)

    def test_dim_gaussian_prediction_compute_value_cov(self):
        A = np.array([[1.0,2.0,3.0],[4.0,5.0,6.0],[7.0,8.0,9.0]])
        f = pfg.LinearMatrixWrapper(A)
        covariance = TestHelpers.generateRandomCovarianceMatrix(3)
        gm = pfg.GaussianPrediction(f=f,covariance=covariance)
        x = np.array([4.0,5.0,6.0])
        vals = np.array([gm.compute(x) for _ in range(100000)])

        mean = np.mean(vals, axis=0)
        cov = np.cov(vals,rowvar=False)
        np.testing.assert_almost_equal(mean, f(x), 1e-2)
        np.testing.assert_almost_equal(cov, covariance, 1e-1)

    def test_unscaled_probability_from(self):
        A = np.array([[1.,2.,3.],[4.,5.,6.]])
        hx2z = pfg.LinearMatrixWrapper(A)
        z = np.array([1.,2.])
        covariance_inv= TestHelpers.generateRandomCovarianceMatrix(2)
        covariance = np.linalg.inv(covariance_inv)

        x = np.array([4.0,5.0,6.0])

        gm = pfg.GaussianMeasurement(hx2z=hx2z,z=z,covariance_inv=covariance_inv)
        # TODO finish test


if __name__ == '__main__':
    unittest.main()
